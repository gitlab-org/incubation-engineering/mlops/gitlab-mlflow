# gitlab-mlflow

First class MLFlow integration for GitLab

https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/9

## Disclaimer: Gitlab-MLFlow is an Incubation Project!

As a project driven by Incubation Engineering, API stability is not guaranteed and some performance issues will likely
be encountered. 

More about MLOps Incubation Engineering: https://about.gitlab.com/handbook/engineering/incubation/mlops/
More about Incubation Engineering: https://about.gitlab.com/handbook/engineering/incubation/

Feedback? Add an issue to this project or a comment on https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/9

## Planned JTBD for MVP

Initial focus is making Tracking Server and basic run tracking work

**For the Platform Engineer**:
- [ ] When setting up MLFlow, I want it to be installed along GitLab, so that I don't spend time configuring them
- [ ] When starting up MLFlow, I want the tracking server started along Gitlab, so that I don't have to do this manually 
- [ ] When accessing MLFlow, I want it to respect the project's permission settings, so that I can have user management 
- [ ] When MLFlow is running, I want it to store run artifacts on GitLab's Object Storage, so that I don't need to configure this on my own

**For the MLFlow consumer**
- [ ] When exploring my model, I want to still be able to use MLFlow Rest api, so my existing code doesn't break
- [ ] When running experiments, I want to track my runs using a gitlab url, so that I minimize changes to my existing code
- [ ] After running the experiments, I want to manage them in my GitLab repository, so that I can compare them and chose the best configuration
- [ ] When browsing experiments, I want to only see experiments and runs for the project I am referring to, so that I can focus on my current use case 

## Installation

## Contributing
